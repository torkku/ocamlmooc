(*
 * Part A: A First Draft
 *
 * Our first goal will be to build such a table and generate sentences from it,
 * quick and dirty style, using lists and their predefined operators. Consider
 * using as much as possible the List module (List.assoc, List.length, List.nth,
 * etc.) and don't think about efficiency.
 *
 * In this exercise, we will use associative lists as the data structure that
 * links each word to its possible suffixes. Associative lists are often used in
 * prototypes or non critical programs because they are very easy to use and
 * debug. Their major downfall is the complexity of searching for an element.
 * The type of an associative list that maps string keys to 'a values is simply
 * (string * 'a) list. The value associated with a key "x" is simply the right
 * component of the first pair in the list whose left component is "x". This
 * lookup is already defined in the standard library as List.assoc. Hence,
 * setting the value of "x" to 3, for instance, is just adding ("x",3) in front
 * of the list. To remove an element, you can just use List.filter with the
 * right predicate.
 *
 * The type of lookup tables for this exercise is
 *
 * type ltable = (string * string list) list
 *
 *)

(*
 * 1.
 *
 * Write a function words : string -> string list that takes a sentence and
 * returns the list of its words. As a first approximation, will work on single
 * sentences in simple english, so you can consider sequences of roman letters
 * and digits as words, and everything else as separators. If you want to build
 * words bit by bit, you can experiment with the Buffer module. Beware, this
 * preliminary function may not be as easy as it seems. 
 *)

let explode str =
  let rec exp a b =
    if a < 0 then b
    else exp (a - 1) (str.[a] :: b)
  in
  exp (String.length str - 1) []
;;

let implode l =
  let result = String.create (List.length l) in
  let rec imp i = function
  | [] -> result
  | c :: l -> result.[i] <- c; imp (i + 1) l in
  imp 0 l
;;

let split input =
    let rec split input curr_word seen_words =
        match input with
          | [] -> let ret = (List.rev curr_word) :: seen_words in List.rev ret
          | c :: cs ->
            if c = ' ' then
                split cs [] ((List.rev curr_word) :: seen_words)
            else
                split cs (c :: curr_word) seen_words
    in split input [] []
;;

let isalnum = function
  | 'a' .. 'z' -> true
  | 'A' .. 'Z' -> true
  | '0' ..  '9' -> true
  | _ -> false
;;

let rec cleanup_string = function
  | [] -> []
  | x :: [] -> x :: []
  | x :: y :: xs ->
     if not (isalnum x) && x = y
     then cleanup_string (y :: xs)
     else x :: cleanup_string (y :: xs)
;;

let words str =
    List.map implode (split (cleanup_string (explode str)))
;;

(*
 * 2.
 *
 * Write build_ltable : string list -> ltable that builds an associative list
 * mapping each word present in the input text to all its possible successors
 * (including duplicates). The table should also contain "START" that points to
 * the first word and "STOP" that is pointed by the last word.
 * For instance, a correct (and minimal) table for "x y z y x y" looks like:
 *
 * build_ltable ["x"; "y"; "z"; "y"; "x"; "y"] ;;
 *
 * [ ("z", [ "y" ]);
 *   ("x", [ "y" ; "y" ]);
 *   "START", [ "x" ]);
 *   ("y", [ "x" ; "z" ; "STOP" ]) ]
 *
 * "I am a man and my dog is a good dog and a good dog makes a good man"
 *
 * Example table produced by above sentence:

[("START", ["i"]), ("I", ["am"]), ("am", ["a"]), ("a", ["man"; "good"]),
 ("man", ["and"; "STOP"]), ("and", ["my"; "a"]), ("my", ["dog"]),
 ("dog", ["s"; "and"; "makes"]), ("good", ["dog", "man"]), ("is", ["a"]), ("makes", ["a"])]

 *
 *)

let build_ltable word_list =

  let assoc_word key value table =
    try
      let assoc_list = List.assoc key table in
      (key, value :: assoc_list) :: List.remove_assoc key table
    with
    | Not_found -> (key, [value]) :: table
  in

  let rec aux table words = match words with
    | [] -> []
    | x :: [] -> assoc_word x "STOP" table
    | x :: y :: xs -> aux (assoc_word x y table) (y :: xs)
  in
  aux [("START", [List.hd word_list])] word_list
;;

(*
 * 3.
 *
 * Write the random selection function next_in_ltable : (string * string list)
 * list -> string - > string which takes a table, a given word and returns a
 * valid successor of this word. Your function should respect the probability
 * distribution (which should be trivially ensured by the presence of the
 * duplicates in the successor lists).
 *
 *)

let next_in_ltable table word =
  let assoc_list = List.assoc word table in
  let n = Random.int (List.length assoc_list) in
  List.nth assoc_list n
;;

(*
 * 4.
 *
 * Write the random generation function walk_ltable : (string * string list)
 * list -> string list which takes a table and returns a sequence of words that
 * form a valid random sentence (without the "START" and "STOP").
 *
 * You can use display_quote: string list -> unit to display the generated
 * texts.
 *
 *  --
 *  This table can then be used to generate new text that ressembles the input
 *  in the following way: starting from the "START" word, choose one of the
 *  words that may appear after it, with the probability found in the table, add
 *  it to the output, then iterate the process until the "STOP" word is found.
 *

build_ltable (words "I am a man and my dog is a good dog and a good dog makes a good man")

[("man", ["and"; "STOP"]); ("good", ["dog"; "dog"; "man"]);
 ("a", ["man"; "good"; "good"; "good"]); ("makes", ["a"]);
 ("dog", ["is"; "and"; "makes"]); ("and", ["my"; "a"]); ("is", ["a"]);
 ("my", ["dog"]); ("am", ["a"]); ("I", ["am"]); ("START", ["I"])]

 *
 *)

let walk_ltable table =
  let rec aux word =
    let next = next_in_ltable table word in match next with
    | "STOP" -> []
    | _ -> next :: aux next
  in
  aux (next_in_ltable table "START")
;;


(*
 * Part B: Performance Improvements
 *
 * Now, we want to use more efficient data structures, so that we can take
 * larger inputs and build bigger transition tables.
 *
 * In this exercise, we will use hash tables, predefined in OCaml in the Hashtbl
 * module. Used correctly, hash table provide both fast insertion and
 * extraction. Have a look at the documentation of the module. In particular,
 * don't miss the difference between Hashtbl.add and Hashtbl.replace (you'll
 * probably want to use the latter most of the time).
 *
 * The types for this exercise are:
 *
 * type distribution =
 *   { total : int ;
 *       amounts : (string * int) list }
 *
 * type htable = (string, distribution) Hashtbl.t
 *
 *)

(*
 * 5.
 *
 * In the simple version, we stored for each word the complete list of
 * suffixes, including duplicates. This is a valid data structure to use when
 * building the table since adding a new suffix in front of the list is fast.
 * But when generating, it means computing the length of this list each time,
 * and accessing its random nth element, which is slow if the list is long.
 *
 * Write compute_distribution : string list -> distribution that takes a list of
 * strings and returns a pair containing its length and an association between
 * each string present in the original list and its number of occurrences.
 *
 * For instance, compute_distribution ["a";"b";"c";"b";"c";"a";"b";"c";"c";"c"]
 * should give { total = 10 ; amounts = [("c", 5); ("b", 3); ("a", 2)] }
 *
 * Hint: a first step that simplifies the problem is to sort the list.
 *
 *)

let compute_distribution (l : string list) : distribution = 
  let rec aux total amount_list l =
    match List.sort compare l with
    | [] -> { total = total; amounts = amount_list }
    | x :: xs -> try
          let amount = List.assoc x amount_list in
          aux (total + 1) ((x, amount + 1) :: List.remove_assoc x amount_list) xs
        with
        | Not_found -> aux (total + 1) ((x, 1) :: amount_list) xs
  in
  aux 0 [] l
;;

(*
 * 6.
 *
 * Write a new version of build_htable : string list -> htable that creates a
 * hash table instead of an associative list, so that both table building and
 * sentence generation will be faster.
 *
 * Like the associative list, the table is indexed by the words, each word being
 * associated to its successors. But instead of just storing the list of
 * successors, it will use the format of the previous question.
 *
 * Hint: You can first define an intermediate table of type (string, string
 * list) Hashtbl.t that stores the lists of successors with duplicates. Then you
 * traverse this intermediate table with Hashtbl.iter, and for each word, you
 * add the result of compute_distribution in the final table.
 *
 * type htable = (string, distribution) Hashtbl.t
 *
 * compute_distribution ["a";"b";"c";"b";"c";"a";"b";"c";"c";"c"]
 * { total = 10 ; amounts = [("c", 5); ("b", 3); ("a", 2)] }
 *
 * type assoc_table = (string, string list) Hashtbl.t
 *

build_htable (words "I am a man and my dog is a good dog and a good dog makes a good man")

Hashtbl.find (build_htable (words "I am a man and my dog is a good dog and a good dog makes a good man")) "I"

 *)

let build_htable word_list : htable =

  let assoc_word table key value =
    try
      let assoc_list = Hashtbl.find table key in
      (Hashtbl.replace table key (value :: assoc_list); table)
    with
    | Not_found -> (Hashtbl.add table key [value]; table)
  in

  let rec compute_word_table table words = match words with
    | [] -> table
    | x :: [] -> assoc_word table x "STOP"
    | x :: y :: xs -> compute_word_table (assoc_word table x y) (y :: xs)
  in
  
  let rec compute_distribution_table table words =
    let word_table = compute_word_table (Hashtbl.create 200) words in
    (Hashtbl.iter 
       (fun word successors ->
          let distribution = compute_distribution successors in 
          Hashtbl.add table word distribution) word_table); table
  in
  let table = Hashtbl.create 200 in
  compute_distribution_table table ("START" :: word_list)
;;


(*
 * 7.
 *
 * Define next_in_htable : htable -> string -> string that does the same thing
 * as next_in_ltable for the new table format.
 *
 *

Hashtbl.find (build_htable (words "I am a man and my dog is a good dog and a good dog makes a good man")) "good" ;;
- : distribution = {total = 3; amounts = [("man", 1); ("dog", 2)]}

 *)

let replicate list n =
  let rec prepend n acc x =
    if n = 0 then acc else prepend (n-1) (x :: acc) x in
  let rec aux acc = function
    | [] -> acc
    | h :: t -> aux (prepend n acc h) t  in
  aux [] (List.rev list)
;;

let next_in_htable table  (word : string) : string =
  let rec build_list l acc =
    match l with
    | [] -> acc
    | t :: ts -> build_list ts ((replicate [(fst t)] (snd t)) @ acc)
  in
  let next lst =
    let n = Random.int (List.length lst) in
    List.nth lst n
  in
  let distribution = Hashtbl.find table word in
  next (build_list distribution.amounts [])
;;

(*
 * 8.
 *
 * Finally, define walk_htable : htable -> string list
 * 
 * 
 
let tbl = 
  build_htable (words "I am a man and my dog is a good dog and a good dog makes a good man") 
  in display_quote (walk_htable tbl)

 * 
 *)

let walk_htable (table : htable) : string list =
  let rec aux word =
    let next = next_in_htable table word in match next with
    | "STOP" -> []
    | _ -> next :: aux next
  in
  aux (next_in_htable table "START")
;;


(*
 * Part C: Quality Improvements
 *
 *)

(*
 * 9.
 *
 * If we want to generate sentences from larger corpuses, such as the ones of
 * the ebooks_corpus given in the prelude, we cannot just ignore the
 * punctuation. We also want to generate text using not only the beginning of
 * the original text, but the start of any sentence in the text.
 *
 * Define sentences : string -> string list list that splits a string into a
 * list of sentences such as:
 *
 * - uninterrupted sequences of roman letters, numbers, and non ASCII
 *   characters (in the range '\128'..'\255') are words;
 * - single punctuation characters ';', ',', ':', '-', '"', '\'', '?', '!' and
 *   '.' are words;
 * - punctuation characters '?', '!' and '.' terminate sentences;
 * - everything else is a separator;
 * - and your function should not return any empty sentence.
 * 
 *)

let sentences (str : string) : string list list = 
  let is_sentence_finished = function
    | '?' | '!' | '.' -> true
    | _ -> false
  in
  let is_word_char = function
    | '\128' .. '\255' -> true 
    | c -> isalnum c
  in
  let is_word = function
    | ';' | ',' | ':' | '-' | '"' | '\'' | '?' | '!' | '.' -> true
    | _ -> false
  in
  let rec to_word (lst : char list) : string = (implode (List.rev lst))
  in
  let rec aux input curr_word curr_sentence seen_sentences = 
    match input with
    | [] -> 
        if (List.length curr_word > 0) then 
          (List.rev ((to_word curr_word) :: curr_sentence)) :: seen_sentences
        else
        if (List.length curr_sentence > 0) then 
          List.rev (curr_sentence :: seen_sentences)
        else 
          List.rev seen_sentences 
    | c :: cs ->
        if (is_word c) then
          if (is_sentence_finished c) then
            if (List.length curr_word > 0) then
              aux cs [] [] ((List.rev ((to_word [c]) :: (to_word (curr_word)) :: curr_sentence)) :: seen_sentences)
            else
              aux cs [] [] ((List.rev ((to_word [c]) :: curr_sentence)) :: seen_sentences)
          else 
          if (List.length curr_word > 0) then
            aux cs [] ((to_word [c]) :: (to_word curr_word) :: curr_sentence) seen_sentences
          else
            aux cs [] ((to_word [c]) :: curr_sentence) seen_sentences
        else
        if (is_word_char c) then 
          aux cs (c :: curr_word) curr_sentence seen_sentences
        else
        if (List.length curr_word > 0) then 
          aux cs [] ((to_word curr_word) :: curr_sentence) seen_sentences
        else 
          aux cs [] curr_sentence seen_sentences

  in aux (explode str) [] [] [] 
;;

(*
 * Now, we will drastically improve the results by matching sequences of more
 * than two words. We will thus update the format of our tables again, and use
 * the following ptable type (which looks a lot like the previous one).
 *
 * type ptable =
 * { prefix_length : int ;
 *   table : (string list, distribution) Hashtbl.t }
 *
 * So let's say we want to identify sequences of N words in the text. The
 * prefix_length field contains N-1. The table field associates each list of
 * N-1 words from the text with the distribution of its possible successors.
 *
 * The table on the right gives the lookup table for the example given at the
 * beginning of the project: 'I am a man and my dog is a good dog and a good
 * dog makes a good man', and a size of 2. You can see that the branching
 * points are fewer and make a bit more sense.
 *
 * As you can see, we will use "STOP" as an end marker as before. But instead
 * of a single "START" we will use as a start marker a prefix of the same size
 * as the others, filled with "START".
 *)

(*
 * 10.
 *
 * First, define start: int -> string list that makes the start prefix for a
 * given size (start 0 = [], start 1 = [ "START" ], start 2 = [ "START" ;
 * "START" ], etc.).
 *
 *)

let rec start n =
  if n = 0 then [] else "START" :: start (n - 1)
;;

(*
 * 11.
 *
 * Define shift: string list -> string -> string list. It removes the front
 * element of the list and puts the new element at the end.
 *
 * shift [ "A" ; "B" ; "C" ] "D" = [ "B" ; "C" ; "D" ]
 *
 * shift [ "B" ; "C" ; "D" ] "E" = [ "C" ; "D" ; "E" ]
 *
 *)

let shift l x =
  (List.tl l) @ [x]
;;

(*
 * 12.
 * 
 * Define build_ptable : string list -> int -> ptable that builds a table for a
 * given prefix length, using the two previous functions.
 *

type distribution =
  { total : int ;
    amounts : (string * int) list }

type ptable =
  { prefix_length : int ;
    table : (string list, distribution) Hashtbl.t }

build_ptable (words "I am a man and my dog is a good dog and a good dog makes a good man") 2

let tbl = build_ptable (words "I am a man and my dog is a good dog and a good dog makes a good man") 2 in Hashtbl.find tbl.table ["START"; "START"]
let tbl = build_ptable (words "I am a man and my dog is a good dog and a good dog makes a good man") 1 in Hashtbl.find tbl.table ["START"]

 *
 *)

let extract_n_items n lst acc =
  if (List.length lst < n) then
    raise (Invalid_argument "extract_n_items: List length is smaller than n")
  else
    let rec aux lst acc prev = match lst with
      | [] -> raise (Invalid_argument "extract_n_items: empty list")
      | x :: [] ->
          (List.rev (x :: acc), "STOP", [])
      | x :: xs ->
          if (List.length acc >= n) then
            ((List.rev acc), (List.hd xs), x :: xs)
          else
          if (List.length acc = n - 1) then
            aux (x :: xs) (x :: acc) x
          else
            aux xs (x :: acc) x
    in
    aux lst acc "STOP"
;;

let assoc_words table key value =
  try
    let assoc_list = Hashtbl.find table key in
    (Hashtbl.replace table key (value :: assoc_list); table)
  with
  | Not_found -> (Hashtbl.add table key [value]; table)
;;

let rec compute_single_prefix_table table words = match words with
  | [] -> table
  | x :: [] -> assoc_words table [x] "STOP"
  | x :: y :: xs -> compute_single_prefix_table (assoc_words table [x] y) (y :: xs)
;;

let rec compute_prefix_table table words n =
  if (List.length words = 0) then
    table
  else
    let prefix, word, rest = extract_n_items n words [] in
    let tbl = assoc_words table prefix word in compute_prefix_table tbl rest n
;;

let build_ptable (word_list : string list) (n : int) : ptable =
  let rec compute_distribution_table table words =

    let rec add_to_table key successors =
      let distribution = compute_distribution successors in
      Hashtbl.add table key distribution
    in
    if (n = 1) then
      let prefix_table = compute_single_prefix_table (Hashtbl.create 200) words in
      (Hashtbl.iter add_to_table prefix_table); table
    else
      let prefix_table = compute_prefix_table (Hashtbl.create 200) words n in
      (Hashtbl.iter add_to_table prefix_table); table

  in
  let tbl = Hashtbl.create 200 in
  let tbl = compute_distribution_table tbl ((start n) @ word_list) in
  { prefix_length = n; table = tbl }
;;

(*
 * 13.
 *
 * Define walk_ptable : ptable -> string list that generates a sentence from a
 * given ptable. Unless you put specific annotations, next_in_htable should be
 * polymorphic enough to work on the field table of a ptable, so you don't have
 * to rewrite one. If you want, since we now have proper sentence splitting,
 * you can generate multi-sentence texts, by choosing randomly to continue from
 * the start after encountering a "STOP".
 *
 *)

(*
 *

let tbl = build_ptable (words "I am a man and my dog is a good dog and a good dog makes a good man") 2 in next_in_ptable tbl.table ["a"; "good"] ;;
- : string = "dog"
let tbl = build_ptable (words "I am a man and my dog is a good dog and a good dog makes a good man") 2 in next_in_ptable tbl.table ["a"; "good"] ;;
- : string = "man"

let tbl = build_ptable (words "I am a man and my dog is a good dog and a good dog makes a good man") 2 in Hashtbl.find tbl.table ["START"; "START"] ;;
- : distribution = {total = 1; amounts = [("I", 1)]}

let tbl = build_ptable (words "I am a man and my dog is a good dog and a good dog makes a good man") 2 in Hashtbl.find tbl.table ["START"; "I"] ;;
- : distribution = {total = 1; amounts = [("am", 1)]}

 *)


let next_in_ptable table (key : string list) : string =
  let rec build_list l acc =
    match l with
    | [] -> acc
    | t :: ts -> build_list ts ((replicate [(fst t)] (snd t)) @ acc)
  in
  let next lst =
    let n = Random.int (List.length lst) in
    List.nth lst n
  in
  let distribution = Hashtbl.find table key in
  next (build_list distribution.amounts [])
;;

let walk_ptable ({ table; prefix_length = pl } : ptable) =
  let rec aux (key : string list) =
    let next = next_in_ptable table key in match next with
    | "STOP" -> []
    | _ ->
        next ::
        if (pl = 2) then
          aux ([(List.hd (List.rev key)); next])
        else
          aux ([next])
  in
  aux (start pl)
;;

(*
 * 14.
 *
 * Define merge_ptables: ptable list -> ptable that combines several tables
 * together (you may fail with an exception if the prefix sizes are
 * inconsistent).
 *
 *)

let merge_hashtbl t1 t2 =
  let merge key value =
    try
      let distribution = Hashtbl.find t1 key in
      let mergeval = {
        total = value.total + distribution.total;
        amounts = List.merge compare value.amounts distribution.amounts
      } in
      Hashtbl.replace t1 key mergeval
    with
    | _ -> Hashtbl.add t1 key value
  in
  Hashtbl.iter merge t2; t2
;;

let merge_ptables (table_list : ptable list) : ptable =

  let rec aux temp_ptable table_list prefix_length =
    match table_list with
    | [] -> { table = temp_ptable; prefix_length = prefix_length }
    | t :: ts ->
        if t.prefix_length <> prefix_length then
          raise (Invalid_argument "merge_ptables: prefix_length mistmatch")
        else
          merge_hashtbl temp_ptable t.table; aux temp_ptable ts prefix_length
  in
  let temp_ptable = Hashtbl.create 100 in
  let prefix_length = (List.hd table_list).prefix_length in
  aux temp_ptable table_list prefix_length
;;

