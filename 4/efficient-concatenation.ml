(*

Concatenating two standard OCaml lists takes a time proportional to the length
of the first list. In this exercise, we implement a data structure for lists
with a constant time concatenation.
The preludes gives a type 'a clist, which is either a single element of type 'a,
the concatenation of two 'a clist or an empty 'a clist.

This representation of a list is not linear: it is a tree-like datastructure
since the CApp constructor contains two values of type 'a clist.

The sequence of elements contained in a value of type 'a clist is obtained by a
depth-first traversal of the tree. For instance, the example given in the
prelude, of type int clist is a valid representation for the sequence [1;2;3;4].

--
Write a function to_list : 'a clist -> 'a list which computes the 'a list that
contains the same elements as the input 'a clist, in the same order.

--
Write a function of_list : 'a list -> 'a clist which computes the 'a clist that
contains the same elements as the input 'a list, in the same order.

--
Write a function append : 'a clist -> 'a clist -> 'a clist such that:

- append CEmpty l = append l CEmpty = l
- append l1 l2 = CApp (l1, l2) otherwise

--
Write a function hd : 'a clist -> 'a option that returns Some x where x is the
first element of the input 'a clist, if it is not empty, and returns None
otherwise.

--
Write a function tl : 'a clist -> 'a clist option that returns Some l where l is
the input sequence without its first element, if this input sequence is not
empty, or returns None otherwise.

THE GIVEN PRELUDE

type 'a clist =
  | CSingle of 'a
  | CApp of 'a clist * 'a clist
  | CEmpty

let example =
  CApp (CApp (CSingle 1,
              CSingle 2),
        CApp (CSingle 3,
              CApp (CSingle 4, CEmpty)))

*)

let to_list cl = 
  let rec aux cl list = match cl with
    | CEmpty -> list
    | CSingle i -> i :: list
    | CApp (l, r) -> (aux l list) @ (aux r list)
  in aux cl []
;;

(*
to_list (CApp (CSingle 1, CEmpty) -> [1]
to_list (CApp (CSingle 1, CApp (CSingle 2, CEmpty))) -> [1;2]
to_list (CApp (CSingle 3, (CApp (CSingle 1, CApp (CSingle 2, CEmpty))))) -> [3;1;2]
*)
let rec of_list l =
  let rec aux list cl = match list with
    | [] -> CEmpty
    | x :: [] -> CApp (CSingle x, CEmpty) 
    | x :: xs -> CApp (CSingle x, aux xs cl)
  in aux l CEmpty
;;

(*
append CEmpty l = append l CEmpty = l
append l1 l2 = CApp (l1, l2) otherwise
*)
let append cl1 cl2 = match cl1, cl2 with
  | CEmpty, CEmpty -> CEmpty
  | _ -> CApp (cl1, cl2)
;;

(*
hd : 'a clist -> 'a option

-Some x where x is the first element of the input 'a clist, if it is not empty
-None otherwise
*)
let hd cl = match to_list cl with 
  | [] -> None
  | xs -> Some (List.hd xs)
;;

(*
tl : 'a clist -> 'a clist option
*)
let tl cl = match to_list cl with
  | [] -> None
  | x :: xs -> Some (of_list xs)
;;

