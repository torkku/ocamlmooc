(*

A binary tree t, of the 'a bt type given in the prelude, is either an empty
tree, or the root of a tree with a value and two children subtrees.

--
Write a function height : 'a bt -> int that computes the height of a tree.

--
A tree is balanced if, for all internal node n, its two subtrees have the same
height. Write a function balanced : 'a bt -> bool that tells if a tree is
balanced. 

THE GIVEN PRELUDE

type 'a bt =
  | Empty
  | Node of 'a bt * 'a * 'a bt ;;

*)

let rec height t =
  let rec aux i =  match t with
    | Empty -> 0
    | Node (left, x, right) -> 1 + max (height left) (height right)
  in aux 1
;;

let rec balanced t  = match t with
  | Empty -> true
  | Node (left, x, right) -> 
      let h_left = height left in
      let h_right = height right in
      (balanced left) && (balanced right) &&
      (max h_left h_right) - (min h_left h_right) <= 1
;;

