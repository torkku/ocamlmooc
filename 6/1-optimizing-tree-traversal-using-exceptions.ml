(*

In a previous exercise, we defined binary trees of type 'a bt (given in the
prelude).
A binary search tree is balanced if, for all internal node n, its two subtrees
have the same height.

You wrote then a function height for balanced trees, and a function balanced
that checks whether a tree is balanced, that looked like the ones given in the
template.
This code is quite elegant, but not the most efficient. In this exercise we will
improve it using exceptions. 

--
Without changing their algorithms, instrument height and balanced to make them
return also the number of visits of leaves (each encounter of an Empty
constructor counts for 1, Nodes are not counted). The functions should now be
typed height : 'a bt -> int * int and balanced : 'a bt -> bool * int where the
right part of the result pair is the number of visits.
When rewriting balanced, don't forget to include the results of all the calls to
height in the total sum. You should also remember that if the left hand side of
a (&&) is false, the rest is not executed, so be cautious in your rewriting to
have the same behaviour (adding ifs if necessary), otherwise you could end up
calling height more than the original version.

--
We now define an exception Unbalanced of int, that we use in a function
bal_height : 'a bt -> int * int that returns the height of a balanced tree,
with the number of visits of leaves and raises Unbalanced n if the input
tree is not balanced. The exception must be raised as soon as possible, when
the first pair of subtrees with different heights is encountered. The
parameter n of the exception is the number of visits of leaves up to the
exception. You may have to define an auxiliary recursive function that takes
an additional parameter containing the number of visits already done during
the start of the traversal.

--
Define now an improved version balanced_fast : 'a bt -> bool * int, that uses
bal_height and returns a boolean indicating whether the input tree is balanced,
and the number of visits.

THE GIVEN PRELUDE

type 'a bt =
      | Empty
        | Node of 'a bt * 'a * 'a bt ;;

exception Unbalanced of int ;;

*)

let rec height' = function
  | Empty -> 0
  | Node (t, _, t') -> 1 + (max (height' t) (height' t')) ;;

let rec height = function
  | Empty -> (0, 1)
  | Node (Empty, _, Empty) -> (1, 2)
  | Node (t, _, Empty)|Node (Empty, _, t) -> let h, c = height t in h + 1, c + 1
  | Node (t, _, t') -> 
      let h, c = height t in
      let h', c' = height t' in
      if h > h' then
        h + 1, c + c'
      else
        h' + 1, c' + c
;;

let rec balanced' = function
  | Empty -> true
  | Node (t, _, t') ->
      (balanced' t) && (balanced' t') && height t = height t' ;;

let rec balanced = function
  | Empty -> true, 1 
  | Node (t, _, t') ->
      let b, count = balanced t in
      if b then
        let b', count' = balanced t' in
        if b' then
          let h, leaves = height t in
          let h', leaves' = height t' in
          if h = h' then
            true, count + count' + leaves + leaves'
          else
            false, count + count' + leaves + leaves'
        else
          false, count + count'
      else
        false, count
;;

let rec bal_height bst = 
  let rec helper n = function
    | Empty -> (0, n+1)
    | Node (Empty, _, Empty) -> (1, n+2)
    | Node (t, _, Empty)|Node (Empty, _, t) -> raise (Unbalanced (n+1))
    | Node (t, _, t') -> 
        let h, l = helper n t in
        let h', l' = helper n t' in 
        if h != h' then
          raise (Unbalanced (n + l + l'))
        else
          h + 1, l + l'
  in helper 0 bst
;;

let balanced_fast bst =
  try 
    let h, c = bal_height bst in true, c
  with
    Unbalanced n -> false, n
;;

