(*
In this exercise, we will do some basic two-dimensional geometry.

We represent a point in two dimensions using a pair, as defined by type point2d in the given prelude. The first component is abscissa (x) and the second component is the ordinate (y). Abscissas grow from left to right and ordinates grow from bottom to top as illustrated by the following schema:

                          ^ (y)
                          |
                          |
                          |
                          |
   -------------------- (0,0) --------------------> (x)
                          |
                          |
                          |
                          |

A tetragon is a polygon with four sides. We represent such an object using a 4-uple of points, as defined by type tetragon in the given prelude. that appear in the following order: the left upper point (lup), the right upper point (rup), the left lower point (llp) and the right lower point (rlp).

In the sequel, we assume that all the points are pairwise distinct.

    Write a function pairwise_distinct of type tetragon -> bool that checks that the points of an input tetragon are pairwise distinct.
    A tetragon is well-formed if the following properties are verified:
        The left upper and the left lower points have the lowest abscissa.
        Between the two leftmost points, the left upper point has the greatest ordinate.
        Between the two rightmost points, the right upper point has the greatest ordinate. 
    Write a function wellformed of type tetragon -> bool that returns true if and only if the input tetragon is well formed.
    A simple way to rotate a tetragon by 90 degrees clockwise with respect to (0, 0) is to rotate each of its points by exchanging their abscissa and ordinate and negating the resulting ordinate.
    Write a function rotate_point of type point2d -> point2d such that rotate_point p is the point p rotated as explained in the previous paragraph.
    Once rotated, the points of the tetragon may not be in the right order: lup may be now llp, rup may be now llp, etc.
    Write a function reorder of type point2d * point2d * point2d * point2d -> tetragon that takes 4 pairwise distinct points and returns a wellformed tetragon.
    Write a function rotate_tetragon that takes a well-formed tetragon and returns a well-formed rotated tetragon.

The given prelude

type point2d = int * int
type tetragon = point2d * point2d * point2d * point2d
*)

let pairwise_cmp cmp (lup, rup, llp, rlp) =
  if cmp lup rup || cmp lup llp || cmp lup rlp ||
     cmp rup llp || cmp rup rlp ||
     cmp llp rlp then
    false
  else
    true;;

let pairwise_distinct (lup, rup, llp, rlp) =
  pairwise_cmp (=) (lup, rup, llp, rlp);; 

let abscissa (x, _) = x;;
let ordinate (_, y) = y;;

let wellformed (lup, rup, llp, rlp) =
  
  (* The left upper and the left lower points have the lowest abscissa. *) 
  if abscissa lup <> abscissa llp || abscissa lup >= abscissa rup || abscissa lup >= abscissa rlp then
    false
  else 
  (* Between the two leftmost points, the left upper point has the greatest ordinate. *)
  if ordinate lup < ordinate llp then
    false
  else 
    
  (* Between the two rightmost points, the right upper point has the greatest ordinate. *)
  if ordinate rup <= ordinate rlp then
    false
  else
    true
;;


(*
    rotate each of its points by exchanging their abscissa and ordinate and 
    negating the resulting ordinate.
*)
let rotate_point (point : point2d): point2d = 
  (ordinate point, -abscissa point);;

(* 
   Write a function reorder of type point2d * point2d * point2d * point2d -> tetragon 
   that takes 4 pairwise distinct points and returns a wellformed tetragon. 
   
   Just sort the abscissas and ordinates correctly according to the 
   specification of wellformedness
   
   how about finding the two points with the smallest abscissa, then compare the 
   ordinates, and put them in the right order ? You don't need to explicitly 
   write all permutations.
   
    The left upper and the left lower points have the lowest abscissa.
    - Between the two leftmost points, the left upper point has the greatest ordinate.
    - Between the two rightmost points, the right upper point has the greatest ordinate.
*)
let rec reorder (lup, rup, llp, rlp) = 
  
  (* The *left upper* and the left lower points have the lowest abscissa. *)
  if abscissa lup > abscissa rup then
    reorder (rup, lup, llp, rlp) 
  else
  if abscissa lup > abscissa rlp then
    reorder (rlp, rup, llp, lup)
  else 
  (* The left upper and the *left lower* points have the lowest abscissa. *)
  if abscissa llp > abscissa rlp then
    reorder (lup, rup, rlp, llp)
  else
  if abscissa llp > abscissa rup then
    reorder (lup, rup, rlp, llp)
  else
  if ordinate lup < ordinate llp then 
    reorder (llp, rup, lup, rlp) 
  else
  if ordinate rup < ordinate rlp then
    reorder (lup, rlp, llp, rup)
  else 
    (lup, rup, llp, rlp)
;;

(* 
    Write a function rotate_tetragon that takes a well-formed tetragon and 
    returns a well-formed rotated tetragon
*)
let rotate_tetragon (lup, rup, llp, rlp) =
  reorder (rotate_point lup, rotate_point rup, rotate_point llp, rotate_point rlp);;

