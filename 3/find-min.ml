(*
 Consider a non empty array of integers a.

    Write a function min : int array -> int that returns the minimal element of a.
    Write a function min_index : int array -> int that returns the index of the minimal element of a.
*)

let min a = 
  Array.fold_left (fun x y -> if x < y then x else y) a.(0) a;;
    
let min_index a = 
  let rec aux i min_idx = 
    if i >= Array.length a then
      min_idx
    else
    if a.(i) < a.(min_idx) then
      aux (i + 1) i
    else
      aux (i + 1) min_idx
  in aux 0 0;;
  
let min_index_old a = 
  Array.fold_left 
    (fun x y -> if a.(x) < a.(y) then x else y) 
    ((Array.length a) - 1)
    (Array.init (Array.length a) (fun n -> n));;

let it_scales =
  "no";;

