(*
Write a function is_sorted : string array -> bool which checks if the values
of the input array are sorted in strictly increasing order, implying that
its elements are unique (use String.compare).

Using the binary search algorithm, an element can be found very quickly
in a sorted array.

Write a function find : string array -> string -> int such that find
arr word is the index of the word in the sorted array arr if it
occurs in arr or -1 if word does not occur in arr.

The number or array accesses will be counted, to check that you
obtain the expected algorithmic complexity. Beware that you
really perform the minimal number of accesses. For instance, if
your function has to test the contents of a cell twice, be
sure to put the result of the access in a variable, and then
perform the tests on that variable.
*)

let is_sorted a =
  if Array.length a < 2 then
    true
  else
    let rec aux i =
      if i < (Array.length a) - 1 then 
        let () = Printf.printf "i %i compare '%s' and '%s'" i a.(i) a.(i + 1) in
        let res = String.compare a.(i) a.(i + 1) in
        if res <> -1 then
          false
        else 
          aux (i + 1)
      else
        true
    in aux 0;;

(*

XXX: Why does not this terminate when doing check???

let rec binary_search a value low high =
  if high = low then
    if a.(low) = value then
      low
    else
      -1
  else
    let mid = (low + high) / 2 in 
    let mid_value = a.(mid) in
    if mid_value > value then
      binary_search a value low (mid - 1)
    else
    if mid_value < value then
      binary_search a value (mid + 1) high
    else
      mid;;
*)

let rec binary_search a value low high =
  if high = low then
    if a.(low) = value then
      low
    else
      -1
  else
    let mid = (low + high) / 2 in 
    if a.(mid) > value then
      binary_search a value low (mid - 1)
    else
    if a.(mid) < value then
      binary_search a value (mid + 1) high
    else
      mid;;

let find dict word =
  if (Array.length dict) = 0
  then 
    -1
  else
    binary_search dict word 0 (Array.length dict - 1);;

  
