(*
The given prelude defines three types, one for three dimensional points, another for velocity vectors in three dimensions, and another one representing moving objects in space.

    Write a function move : point -> dpoint -> point such that move p dp is the point p whose coordinates have been updated according to dp.
    (x is now x +. dx, y is now y +. dy, z is now z +. dz.
    Write a function next : physical_object -> physical_object such that next o is the physical object o at time t + dt.
    The position of next o is the position of o moved according to its velocity vector.
    Suppose that these objects are spheres whose radius is 1.0.
    Write a function will_collide_soon : physical_object -> physical_object -> bool that tells if at the next instant, the two spheres will intersect.

The given prelude

type point  = { x : float; y : float; z : float }
type dpoint = { dx : float; dy : float; dz : float }
type physical_object = { position : point; velocity : dpoint }
*)

let move p dp =
  { x = p.x +. dp.dx; y = p.y +. dp.dy; z = p.z +. dp.dz; };;

(* Write a function next : physical_object -> physical_object 
   such that next o is the physical object o at time t + dt.
   
   The position of next o is the position of o moved according 
   to its velocity vector. *)
let next obj =
  { position = move obj.position obj.velocity; velocity = obj.velocity };; 

(* Suppose that these objects are spheres whose radius is 1.0. 
   Write a function will_collide_soon : physical_object -> physical_object -> bool
   that tells if at the next instant, the two spheres will intersect.
   
   (x2-x1)^2 + (y1-y2)^2 <= (r1+r2)^2
*)
let will_collide_soon p1 p2 =
  let next_p1 = (next p1).position in
  let next_p2 = (next p2).position in
  let radius = 1.0 in
  let distance = sqrt (
      (next_p2.x -. next_p1.x)**2.0 +.
      (next_p2.y -. next_p1.y)**2.0 +.
      (next_p2.z -. next_p1.z)**2.0) 
  in distance < radius *. 2.0;;


let will_collide_soon2 p1 p2 = 
  let next_p1 = next p1 in
  let next_p2 = next p2 in
  let x1 = next_p1.position.x in
  let x2 = next_p2.position.x in
  let y1 = next_p1.position.y in
  let y2 = next_p2.position.y in
  let r = 1.0 in
  if (x2 -. x1)**2.0 +. (y1 -. y2)**2.0 <= r *. 2.0 then
    true
  else 
    false;;

